export class MissingWeddingError {

    message:string;
    description:string = "This error means a wedding could not be found";

    constructor(message:string){
        this.message = message;
    }
}

export class MissingExpenseError {

    message:string;
    description:string = "This error means an expense could not be found";

    constructor(message:string){
        this.message = message;
    }
}

