import{Client} from 'pg'; 
require('dotenv').config({path:'/Users/sandra.muszynski/Desktop/wedding-app/tests/app.env'})

export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:process.env.DATABASENAME,
    port:5432,
    host:'35.236.105.99'
})
client.connect()

// const os = require('os');
// console.log('Platform: ${os.platform()}');