import express from "express";
import cors from 'cors';
import WeddingService from "./services/wedding-service";
import { WeddingServiceImpl } from "./services/wedding-service-impl";
import { Expense, Wedding } from "./entities";
import { MissingExpenseError, MissingWeddingError } from "./errors";
import ExpenseService from "./services/expense-service";
import { ExpenseServiceImpl } from "./services/expense-service-impl";

const app = express();
app.use(express.json()); 
app.use(cors());


const weddingService:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

//POST /weddings
app.post("/weddings", async (req,res) =>{
    console.log("posting a wedding")
    let wedding:Wedding = req.body;
    console.log(wedding);
    wedding = await weddingService.createWedding(wedding);
    res.status(201);
    res.send(wedding);
});

//GET /weddings
app.get("/weddings", async (req, res) => {
    try{
    const weddings:Wedding[] = await weddingService.retrieveAllWeddings();
    res.send(weddings);
    } catch(error){
        if(error instanceof MissingWeddingError){
            res.status(404);
            res.send(error);
        }
    }
});

// GET /weddings/:id
app.get("/weddings/:id", async (req, res)=>{
    try {
        const weddingId = Number(req.params.id);
        const wedding:Wedding = await weddingService.retrieveWeddingById(weddingId);
        res.send(wedding);
        } 
        catch (error) {
        if(error instanceof MissingWeddingError){
            res.status(404);
            res.send(error);
        }}
});

// GET /weddings/:id/expenses
app.get("/weddings/:id/expenses", async (req, res)=>{
    try {
        const weddingId = Number(req.params.id);
        const expenses:Expense[] = await expenseService.retrieveExpensesByWeddingId(weddingId);
        res.send(expenses);
        } 
        catch (error) {
        if(error instanceof MissingWeddingError){
            res.status(404);
            res.send(error);
        }}
});

// DELETE /wedding/:id
app.delete("/wedding/:id", async (req, res) =>{
    try{
        const weddingId = Number(req.params.id);
        let result:Boolean = await weddingService.removeWedding(weddingId);
        res.send(result);
    }
    catch(error){
        if(error instanceof MissingWeddingError){
        res.status(404);
        res.send(error);
    }}
});

// PUT weddings/:id
app.put("/weddings/:id", async (req, res)=>{
    try{
        const wedding_id:number = Number(req.params.id);
        const wedding:Wedding = req.body;
    const updateWedding = await weddingService.updateWedding(wedding);

    res.send(updateWedding);
    }catch(error){
        if(error instanceof MissingWeddingError){
        res.status(404);
        res.send(error);
    }}
});

// GET /expenses
app.get("/expenses", async (req, res) => {
    // try{
        const result:Expense[] = await expenseService.retrieveAllExpenses();
    res.send(result);
    // }catch(error){
    //     if (error instanceof MissingExpenseError){
    //         res.status(404);
    //         res.send(error);
    //     }
    // }
});

// POST /expense
app.post("/expense", async (req,res) =>{
    let expense:Expense = req.body;
    expense = await expenseService.createExpense(expense);
    res.status(201);
    res.send(Expense);
});

// PUT /expenses/:id
app.put("/expenses/:id", async (req, res)=>{
    const expenseId = Number(req.params.id);
    let expense:Expense = req.body;
    expense = await expenseService.updateExpense(expense);
    res.send(expense);
});

// DELETE /expenses/:id
app.delete("/expenses/:id", async (req, res) =>{
    const expenseId = Number(req.params.id);
    let result:Boolean = await expenseService.deleteExpense(expenseId);
    res.send(result);
});

app.listen(3000,()=>{console.log("Application Started")})




