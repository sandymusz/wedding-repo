import { Wedding } from "../entities";

export interface WeddingDAO{

    //CREATE
    createWedding(wedding:Wedding):Promise<Wedding>;
    
    //READ
    getAllWeddings():Promise<Wedding[]>;
    getWeddingById(weddingId:number):Promise<Wedding>;
   
    //UPDATE
    updateWedding(wedding:Wedding):Promise<Wedding>;
   
    //DELETE
    deleteWedding(weddingId:number):Promise<boolean>;
}