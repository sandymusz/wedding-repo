import { client } from "../connection";
import { Expense } from "../entities";
import { MissingExpenseError } from "../errors";
import { ExpenseDAO } from "./expense-dao";


export class ExpenseDaoPostgres implements ExpenseDAO{

    async createExpense(expense: Expense): Promise<Expense> {
        const sql:string = "insert into expense(reason,amount,wed_id) values ($1,$2,$3) returning expense_id;";
        const values = [expense.reason,expense.amount,expense.weddingId];
        const result = await client.query(sql,values);
        expense.expenseId = result.rows[0].expense_id;
        return expense;
    }

    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = 'select * from expense';
        const result = await client.query(sql);
        const expenses:Expense[] = []
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.expense_id,
                row.reason,
                row.amount,
                row.wed_id);
            expenses.push(expense);
        }
        return expenses;
    }
    
    async getExpensesByWeddingId(weddingId: number): Promise<Expense[]> {
        const sql:string = 'select * from expense where wed_id=$1';
        const value = [weddingId];
        const result = await client.query(sql,value);
        const expenses:Expense[] = []
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.expense_id,
                row.reason,
                row.amount,
                row.wed_id);
            expenses.push(expense);
        }
        return expenses;
    }

    async updateExpense(expense: Expense): Promise<Expense> {
        const sql:string = 'update expense set reason=$1, amount=$2, wed_id=$3 where expense_id=$4';
        const values = [expense.reason,expense.amount,expense.weddingId,expense.expenseId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingExpenseError(`The expense with ID ${expense.expenseId} doed not exist`);
        }
        return expense;
    }
    async deleteExpense(expenseId: number): Promise<boolean> {
        const sql:string = 'delete from expense where expense_id=$1';
        const values = [expenseId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingExpenseError(`The expense with ID ${expenseId} does not exist`);
        }
        return true;
    }

}