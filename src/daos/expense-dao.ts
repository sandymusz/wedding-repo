import { Expense } from "../entities";

export interface ExpenseDAO{

    //CREATE
    createExpense(expense:Expense):Promise<Expense>;

    //READ
    getAllExpenses():Promise<Expense[]>;
    
    getExpensesByWeddingId(weddingId:number):Promise<Expense[]>;
    
    //UPDATE
    updateExpense(expense:Expense):Promise<Expense>;

    //DELETE
    deleteExpense(expenseId:number):Promise<boolean>;
    
}