import { client } from "../connection";
import { Wedding, Expense } from "../entities";
import { MissingWeddingError } from "../errors";
import { WeddingDAO } from "./wedding-dao";


export class WeddingDaoPostgres implements WeddingDAO{
    
    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "insert into wedding(event_date,event_location,budget,client_name) values ($1,$2,$3,$4) returning wedding_id;";
        const values = [wedding.date,wedding.location,wedding.budget,wedding.name];
        const result = await client.query(sql,values);
        console.log(sql);
        console.log(result.rows[0]);
        wedding.weddingId = result.rows[0].wedding_id;
        return wedding;
    }

    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = 'select * from wedding';
        const result = await client.query(sql);
        const weddings:Wedding[] = []
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.wedding_id,
                row.event_date,
                row.event_location,
                row.budget,
                row.client_name);
            weddings.push(wedding);
        }
        return weddings;
    }
    
    async getWeddingById(weddingId: number): Promise<Wedding> {
        const sql:string = 'select * from wedding where wedding_id = $1';
        const values = [weddingId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingWeddingError(`The wedding with ID ${weddingId} does not exist`);
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding(
            row.wedding_id,
            row.event_date,
            row.event_location,
            row.budget,
            row.client_name);
        return wedding;
    }
    
    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'update wedding set event_date=$1, event_location=$2, budget=$3, client_name=$4 where wedding_id=$5';
        const values = [wedding.date,wedding.location,wedding.budget,wedding.name,wedding.weddingId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingWeddingError(`The wedding with ID ${wedding.weddingId} doed not exist`);
        }
        return wedding;
    }

    async deleteWedding(weddingId: number): Promise<boolean> {
        const sql:string = 'delete from expense where wed_id=$1';
        const values = [weddingId];
        await client.query(sql,values);

        const sql2:string = 'delete from wedding where wedding_id=$1';
        const values2 = [weddingId];
        await client.query(sql2,values2);
        console.log("returning true");
        return true;
    }
}