export class Wedding{
    constructor(
        public weddingId:number,
        public date:string,
        public location:string,
        public budget:number,
        public name:string
    ){}
}

export class Expense{
    constructor(
        public expenseId:number,
        public reason:string,
        public amount:number,
        public weddingId:number
    ){}
}

