import { Expense } from "../entities";

export default interface ExpenseService{
    
    createExpense(expense:Expense):Promise<Expense>;
  
    retrieveAllExpenses():Promise<Expense[]>;
    retrieveExpensesByWeddingId(weddingId:number):Promise<Expense[]>;

    updateExpense(expense:Expense):Promise<Expense>;
   
    deleteExpense(expenseId:number):Promise<boolean>;
}