import { Wedding } from "../entities";

export default interface WeddingService{

    createWedding(wedding:Wedding):Promise<Wedding>;

    retrieveAllWeddings():Promise<Wedding[]>;
    retrieveWeddingById(weddingId:number):Promise<Wedding>;
    
    updateWedding(wedding:Wedding):Promise<Wedding>;
    
    removeWedding(weddingId:number):Promise<boolean>;
}