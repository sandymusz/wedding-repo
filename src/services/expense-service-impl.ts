import ExpenseService from "./expense-service";
import { ExpenseDaoPostgres } from "../daos/expense-dao-postgres";
import { Expense } from "../entities";
import { ExpenseDAO } from "../daos/expense-dao";


export class ExpenseServiceImpl implements ExpenseService{

    expenseDAO:ExpenseDAO = new ExpenseDaoPostgres()

    createExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.createExpense(expense);
    }
    retrieveAllExpenses(): Promise<Expense[]> {
        return this.expenseDAO.getAllExpenses();
    }
    
    retrieveExpensesByWeddingId(wedding: number): Promise<Expense[]> {
        return this.expenseDAO.getExpensesByWeddingId(wedding);
    }
    updateExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.updateExpense(expense);
    }
    deleteExpense(expenseId: number): Promise<boolean> {
        return this.expenseDAO.deleteExpense(expenseId);
    }
}