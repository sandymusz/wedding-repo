import { WeddingDAO } from "../daos/wedding-dao";
import WeddingService from "./wedding-service";
import { WeddingDaoPostgres } from "../daos/wedding-dao-postgres";
import { Wedding, Expense } from "../entities";
import { MissingWeddingError } from "../errors";


export class WeddingServiceImpl implements WeddingService{

    weddingDAO:WeddingDAO = new WeddingDaoPostgres()

    createWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }
    retrieveAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.getAllWeddings();
    }
    retrieveWeddingById(weddingId: number): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(weddingId);
    }
    async updateWedding(wedding:Wedding): Promise<Wedding> {
        const testWeddingId:Wedding = await this.weddingDAO.getWeddingById(wedding.weddingId)
        if(testWeddingId.weddingId != wedding.weddingId){
            throw Error(`Wedding ID not matched`)
        }
        return this.weddingDAO.updateWedding(wedding);
    }
    
    async removeWedding(weddingId: number): Promise<boolean> {
        const wedding:Wedding = await this.weddingDAO.getWeddingById(weddingId)
        if(weddingId != wedding.weddingId){
            throw new MissingWeddingError(`No Wedding with the ID of ${weddingId}`)
        }
        console.log("passed ID test")
        return this.weddingDAO.deleteWedding(weddingId);
    }
}