import { WeddingDAO } from "../src/daos/wedding-dao";
import { Wedding } from "../src/entities";
import { WeddingDaoPostgres } from "../src/daos/wedding-dao-postgres";


const weddingDAO:WeddingDAO = new WeddingDaoPostgres();

const testWedding:Wedding = new Wedding(0,'4.23.22','Santa Barbara',50000,'Fish');

test("Create a Wedding", async ()=>{
    const result:Wedding = await weddingDAO.createWedding(testWedding);
    expect(result.weddingId).not.toBe(0);
});

test("Get all weddings", async ()=>{
    let wedding1:Wedding = new Wedding(0, '08.28.2021', 'Chicago, IL', 65000, 'Mora');
    let wedding2:Wedding = new Wedding(0, '09.04.2022', 'Los Angeles, CA', 95000, 'Adams');
    let wedding3:Wedding = new Wedding(0, '06.02.2022', 'Park City, UT', 65000, 'Merrin');
    await weddingDAO.createWedding(wedding1);
    await weddingDAO.createWedding(wedding2);
    await weddingDAO.createWedding(wedding3);
});


test("Get wedding by Id", async ()=>{
    let wedding:Wedding = new Wedding(0, "4.23.2022", "Santa Barbara, CA", 50000, "Fish");
    wedding = await weddingDAO.createWedding(wedding);

    let retievedWedding:Wedding = await weddingDAO.getWeddingById(wedding.weddingId);
    expect(retievedWedding.name).toBe(wedding.name);
});

test("update wedding", async ()=>{
    let wedding:Wedding = new Wedding(0,'04.23.2022','Santa Barbara, CA',50000,'Fish');
    wedding = await weddingDAO.createWedding(wedding);

    wedding.date = '04.23.2023';
    wedding = await weddingDAO.updateWedding(wedding)   
    
    expect(wedding.date).toBe('04.23.2023');
});

test("delete wedding by id", async ()=>{
    let wedding:Wedding = new Wedding(0,'04.23.2022','Santa Barbara, CA',50000,'Fish');
    wedding = await weddingDAO.createWedding(wedding);

    const result:boolean = await weddingDAO.deleteWedding(wedding.weddingId);
    expect(result).toBeTruthy()
});

