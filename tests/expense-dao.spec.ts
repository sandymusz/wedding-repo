import { ExpenseDAO } from "../src/daos/expense-dao";
import { Expense } from "../src/entities";
import { ExpenseDaoPostgres } from "../src/daos/expense-dao-postgres";

const expenseDAO:ExpenseDAO = new ExpenseDaoPostgres();

const testExpense:Expense = new Expense(0, 'Flowers', 5000, 1);

test("Create an Expense", async ()=>{
    const result:Expense = await expenseDAO.createExpense(testExpense);
    expect(result.expenseId).not.toBe(0);
});

test("Get all expense", async ()=>{
    let expense1:Expense = new Expense(0, 'Flowers', 5000, 1);
    let expense2:Expense = new Expense(0, 'Flowers', 5000, 1);
    let expense3:Expense = new Expense(0, 'Flowers', 5000, 1);
    await expenseDAO.createExpense(expense1);
    await expenseDAO.createExpense(expense2);
    await expenseDAO.createExpense(expense3);
});

// test("get expenses by wedding Id", async()=>{})



test("update expense", async ()=>{
    let expense:Expense = new Expense(0, 'Flowers', 5000, 1);
    expense = await expenseDAO.createExpense(expense);

    expense.amount = 7000;
    expense = await expenseDAO.updateExpense(expense)   
    
    expect(expense.amount).toBe(7000);
});

test("delete expense by id", async ()=>{
    let expense:Expense = new Expense(0, 'Flowers', 5000, 1);
    expense = await expenseDAO.createExpense(expense);

    const result:boolean = await expenseDAO.deleteExpense(expense.expenseId);
    expect(result).toBeTruthy()
});
